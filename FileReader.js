export class FileReader {
    static EOL = '\n';

    constructor(sourceFileDescriptor) {
        this.fileDescriptor = sourceFileDescriptor;
        this.textDecoder = new TextDecoder();
        this.buffer = new Uint8Array(32);
        this.exhausted = false;
        this.accumulatedText = '';
    }

    async readLine() {
        if (this.exhausted) {
            return null;
        }

        let nextLineIndex = this.accumulatedText.indexOf(FileReader.EOL);

        while (nextLineIndex === -1) {
            const bytes = Number(await this.fileDescriptor.read(this.buffer));
            if (bytes === 0) {
                this.exhausted = true;
                return this.accumulatedText;
            }

            this.accumulatedText += this.textDecoder.decode(this.buffer.subarray(0, bytes));
            nextLineIndex = this.accumulatedText.indexOf(FileReader.EOL);
        }

        const line = this.accumulatedText.substring(0, nextLineIndex);
        this.accumulatedText = this.accumulatedText.substring(nextLineIndex + 1);
        return line;
    }
}
