export class FileWriter {
    constructor(targetFileDescriptor) {
        this.fileDescriptor = targetFileDescriptor;
        this.textEncoder = new TextEncoder();
    }

    async writeLine(line) {
        const buffer = this.textEncoder.encode(`${line}\n`);
        await this.fileDescriptor.write(buffer);
    }
}