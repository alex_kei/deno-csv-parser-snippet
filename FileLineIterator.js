import { FileReader } from './FileReader.js';

export class FileLineIterator {
    constructor(sourceFileDescriptor) {
        this.fileReader = new FileReader(sourceFileDescriptor);
    }

    async next() {
        const line = await this.fileReader.readLine();

        if (line === null) {
            return { done: true };
        }

        return { done: false, value: line };
    }

    [Symbol.asyncIterator] () {
        return this;
    }
}