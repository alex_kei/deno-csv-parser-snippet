const SEPARATOR = ',';

export function getKeys(header) {
    return header.split(SEPARATOR);
}

export function parseLine(line, keys) {
    const values = line.split(SEPARATOR);

    return Object.fromEntries(
        values.map((value, index) => ([keys[index], value])),
    );
}
