import { FileLineIterator } from './FileLineIterator.js';
import { FileWriter } from './FileWriter.js';
import { getKeys, parseLine } from './helpers.js';

function getCommandLineArguments() {
    return {
        source: Deno.args[0] || './data.csv',
        destination: './dest',
    };
}

(async function main() {
    const { source, destination } = getCommandLineArguments();

    const [sourceFile, destinationFile] = await Promise.all([
        Deno.open(source, { read: true }),
        Deno.open(destination, { write: true, create: true }),
    ]);

    const lineIterator = new FileLineIterator(sourceFile);
    const writer = new FileWriter(destinationFile);
    
    const { value: header } = await lineIterator.next();
    const keys = getKeys(header);

    for await (const line of lineIterator) {
        const object = parseLine(line, keys);
        await writer.writeLine(JSON.stringify(object));
    }

    Deno.close(sourceFile.rid);
    Deno.close(destinationFile.rid);
})();
